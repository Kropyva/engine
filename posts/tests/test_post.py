from django.test import TestCase
from django.utils import timezone
from posts.models import Post, Board


class PostTestCase(TestCase):
    def setUp(self):
        test_board = Board.objects.create(uri='test', title='Test')
        Board.objects.create(uri='dest', title='Dest')
        time = timezone.now()
        Post.objects.create(
            id=1,
            time=time,
            board=test_board,
            ip='127.0.0.1',
            sticky=False,
            sage=False,
            locked=False,
            cycle=False
        )
        time = timezone.now()
        for _id in range(2, 10):
            Post.objects.create(
                id=_id,
                thread=1,
                time=time,
                board=test_board,
                ip='127.0.0.1',
                sticky=False,
                sage=False,
                locked=False,
                cycle=False
            )

    def test_thread_moving(self):
        source_board = Board.objects.get(uri='test')
        dest_board = Board.objects.get(uri='dest')
        thread = Post.objects.get(id=1, board=source_board)
        thread_posts = Post.objects.filter(thread=1, board=source_board)
        thread.move(dest_board)
        self.assertEqual(thread.board, dest_board)
        for post in thread_posts:
            self.assertEqual(post.board, dest_board)
