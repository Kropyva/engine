import simplejson as json
from django.test import TestCase
from django.utils import timezone
from django.test.client import RequestFactory
from posts.models import Post, Board
from posts.api import board_threads_api, board_thread_api


class ApiTestCase(TestCase):
    def setUp(self):
        test_board = Board.objects.create(uri='test', title='Test')
        time = timezone.now()
        Post.objects.create(
            id=1,
            time=time,
            board=test_board,
            ip='127.0.0.1',
            sticky=False,
            sage=False,
            locked=False,
            cycle=False
        )
        self.factory = RequestFactory()

    def test_board_threads_api(self):
        test_board = Board.objects.get(uri='test')
        post = Post.objects.get(board=test_board, id=1)
        request = self.factory.get(f'/test/0.json')
        response = json.loads(board_threads_api(request, 'test').content)[0]
        self.assertEqual(list(response.keys()), ['threads', 'page'])
        for thread in response['threads']:
            self.assertEqual(thread['no'], post.id)

    def test_music_file_api(self):
        test_board = Board.objects.get(uri='test')
        time = timezone.now()
        music_file = {
            'error': 0,
            'extension': 'mp3',
            'file': '1482921680005-0.mp3',
            'file_id': '1482921680005-0',
            'file_path': 'm/src/files-0.mp3',
            'filename': ' .mp3',
            'hash': 'asdfkljasdklfjaskldjf',
            'is_an_image': False,
            'name': ' .mp3',
            'size': 605703,
            'thumb': 'file',
            'thumb_path': 'file',
            'thumbheight': 18,
            'thumbwidth': 6,
            'tmp_name': '/tmp/phpkHYDi8',
            'type': 'application/octet-stream'
        }
        op_post = Post.objects.get(board__uri='test', id=1)
        op_post.bump = time
        op_post.save()
        Post.objects.create(
            id=2,
            time=time,
            board=test_board,
            ip='127.0.0.1',
            sticky=False,
            sage=False,
            locked=False,
            cycle=False,
            thread=1,
            files=json.dumps([music_file])
        )
        request = self.factory.get(f'/test/res/1.json')
        json.loads(board_thread_api(request, 'test', 1).content)
