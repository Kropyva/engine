from django.test import TestCase
from posts.templatetags import index

import simplejson as json


class TagTestCase(TestCase):
    def test_invalid_file_data(self):
        file_data = json.dumps(
            [
                {
                    'error': 0,
                    'extension': 'jpg',
                    'file': 'deleted',
                    'file_id': '1501171003276-0',
                    'file_path': 'm/src/1501171003276-0.jpg',
                    'filename': ' .jpg',
                    'hash': 'a8b43ddee9665ea3d4e908c1c1b0235e',
                    'height': 900,
                    'is_an_image': True,
                    'name': ' .jpg',
                    'size': 600594,
                    'thumb_path': 'm/thumb/1501171003276-0.png',
                    'thumbheight': 255,
                    'thumbwidth': 255,
                    'tmp_name': '/tmp/phpV595FV',
                    'type': 'application/octet-stream',
                    'width': 900
                }
             ])
        self.assertEqual(index.get_files(file_data), None)

    def test_valid_file_data(self):
        file_data = json.dumps(
            [
                {
                    'error': 0,
                    'extension': 'jpg',
                    'file': '1501171003276-0.jpg',
                    'file_id': '1501171003276-0',
                    'file_path': 'm/src/1501171003276-0.jpg',
                    'filename': ' .jpg',
                    'hash': 'a8b43ddee9665ea3d4e908c1c1b0235e',
                    'height': 900,
                    'is_an_image': True,
                    'name': ' .jpg',
                    'size': 600594,
                    'thumb_path': 'm/thumb/1501171003276-0.png',
                    'thumbheight': 255,
                    'thumbwidth': 255,
                    'tmp_name': '/tmp/phpV595FV',
                    'type': 'application/octet-stream',
                    'width': 900
                }
             ])
        self.assertEqual(index.get_files(file_data), json.loads(file_data))
