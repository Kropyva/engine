from posts.models import Request


class RequestStoreMiddleware(object):

    """Store each request to database."""

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """Collect request's data"""
        new_request = Request.objects.create(
            path=request.path,
            gets_data=request.GET,
            posts_data=request.POST,
            method=request.method,
            meta_data=request.META
        )
        new_request.save()
        response = self.get_response(request)
        return response
