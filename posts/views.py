# coding: utf-8

"""file with backend code"""

import random
import os
from datetime import timedelta
from collections import Counter, OrderedDict
from contextlib import suppress

import simplejson as json

# django stuff
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.urls import reverse
from django.shortcuts import render
from django.views.static import serve
from django.core.paginator import Paginator
# from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.utils import timezone

# database models
from posts.models import Board, Post, Report

from posts.forms import PostForm, markup
from config.settings import MEDIA_ROOT
from config.settings import config
from config.utils import get_uptime


def error404(request, exception):
    context = {
        'config': config,
        'boards': get_boards_navlist(),
    }
    return render(request, 'posts/404.html', context)


def render_index(request):
    """
    Render main page with lists of boards, recent posts.
    :param request: user's request
    :return: main page
    """
    boards = Board.objects.all().order_by('uri').values()
    posts = Post.objects.values(
        'thread',
        'board',
        'board__uri',
        'board__title',
        'id',
        'body_nomarkup',
        'time'
    ).order_by('-time')[:50]
    try:
        threads_data = set(
            [(post['thread'], post['board']) for post in posts
             if post['thread'] is not None]
        )
        to_fetch = []
        last_threads = []
        for thread in iter(threads_data):
            with suppress(ObjectDoesNotExist):
                # lets check if we already have the thread in our posts
                for post in posts:
                    post_data = (post['id'], post['board'])
                    if thread == post_data:
                        # gotcha!
                        last_threads.append(post)
                        break
                else:
                    to_fetch.append(thread)
        # TODO: Fix it in the Thread update
        # I think there is still a bug, the code can bring collisions between
        # threads (same ids but different boards)
        ids, boards_ = zip(*to_fetch)
        new_threads = Post.objects \
            .filter(thread=None, id__in=ids, board__in=boards_) \
            .values('board__title', 'board__uri', 'body_nomarkup', 'id')
        last_threads.extend(new_threads)
        recent = [post for post in posts[:30][::-1]]
        to_sort = [recent, last_threads]
        for query in to_sort:
            for post in query:
                post['body'] = markup(post['body_nomarkup'],
                                      process_links=False)
        recent = sorted(recent, key=lambda x: x['time'], reverse=True)
    except Exception:  # in case if there are no posts at all
        last_threads = []
        recent = []
    context = {
        'config': config,
        'boards': boards,
        'slogan': random.choice(config['slogan']+[get_uptime()]),
        'last_threads': last_threads,
        'recent_posts': recent
    }
    return render(request, 'posts/main_page.html', context)


def render_stat(request):
    boards = Board.objects.all().order_by('uri').values()
    posts = Post.objects.values('id', 'body_nomarkup', 'thread', 'time',
                                'ip', 'board_id'
                                ).order_by('-time')
    if len(posts):
        stats = make_stats(posts)
    else:
        stats = None
    context = {
        'config': config,
        'boards': boards,
        'stats': stats,
    }
    return render(request, 'posts/statistics.html', context)


def render_board(request, board_name, current_page=1):
    """
    Render board with lists of threads and last 5 posts for them.
    :param request: user's request
    :param board_name: name of board that we should render
    :param current_page: page that user requested
    :return: board page
    """
    boards = Board.objects.all().order_by('uri').values()
    try:
        board = [_ for _ in boards if _['uri'] == board_name][0]
    except IndexError:
        raise Http404("Board doesn't exist")
    board['url'] = board['uri']
    # posts = Post.objects.filter(board__uri=board['uri']).order_by(
    #     '-bump').values()
    thread_count = 100
    pages = Paginator(range(thread_count), 10)
    current_page = int(current_page)
    offset = slice(10*(current_page-1), 10*current_page)
    threads = Post.objects.filter(board__uri=board['uri'], thread=None) \
        .order_by('-bump')[offset].values()
    # [_ for _ in posts if not _['thread']][offset]
    posts = Post.objects.filter(board__uri=board['url'],
                                thread__in=[i['id'] for i in threads]) \
                        .order_by('-time') \
                        .values()

    for thread in threads:
        thread_posts = [_ for _ in posts if _['thread'] == thread['id']]
        posts_len = len(thread_posts)
        thread['omitted'] = posts_len - 5 if posts_len >= 5 else 0
        thread['posts'] = thread_posts[::-1][thread['omitted']:]
    if request.method == 'POST':
        json_response = 'json_response' in request.POST
        agreed_with_rules = 'rules' in request.POST
        form = PostForm(request.POST, request.FILES)
        if agreed_with_rules and form.is_valid():
            _ip = get_ip(request)
            try:
                new_post_id = form.process(board_name, _ip, None)
                return handle_posting_response(
                    new_post_id,
                    json_response,
                    new_post_id,
                    board_name
                )
            except Exception as e:
                return HttpResponse(
                    json.dumps({'error': str(e)}),
                    content_type="application/json"
                )
    else:
        form = PostForm()
    context = {
        'config': config,
        'board': board,
        'boards': boards,
        'random_banner': get_random_banner(board_name),
        'threads': threads,
        'pages': pages,
        'hr': True,
        'index': True,
        'form': form
    }
    return render(request, 'posts/index.html', context)


def render_thread(request, board_name, thread_id):
    """
    Render thread page with all thread's posts.

    :param request: user's request
    :param board_name: name of threads board
    :param thread_id: thread id
    :return: thread page
    """
    boards = Board.objects.all().order_by('uri').values()
    try:
        board = [_ for _ in boards if _['uri'] == board_name][0]
        board['url'] = board['uri']
        post = Post.objects.filter(board__uri=board['uri']).get(id=thread_id)
    except IndexError:
        raise Http404("Board doesn't exist")
    except ObjectDoesNotExist:
        raise Http404("Post doesn't exist")
    post.posts = Post.objects.filter(
        board__uri=board['uri'],
        thread=post.id
    ).order_by('time')
    if request.method == 'POST':
        if 'delete' in request.POST:
            return handle_delete(request.POST, board, thread_id)
        if 'report' in request.POST:
            return handle_report(request, board, thread_id)
        json_response = 'json_response' in request.POST
        agreed_with_rules = 'rules' in request.POST
        post_form = PostForm(request.POST, request.FILES)
        if agreed_with_rules and post_form.is_valid():
            _ip = get_ip(request)
            try:
                new_post_id = post_form.process(board_name, _ip, thread_id)
                return handle_posting_response(
                    new_post_id,
                    json_response,
                    thread_id,
                    board_name
                )
            except Exception as e:
                return HttpResponse(
                    json.dumps({'error': str(e)}),
                    content_type="application/json"
                )
    else:
        post_form = PostForm()

    context = {
        'config': config,
        'random_banner': get_random_banner(board_name),
        'board': board,
        'boards': boards,
        'threads': [post],
        'hr': True,
        'form': post_form,
        'id': 1
    }
    return render(request, 'posts/page.html', context)

def get_random_banner(curr_board):
    banners_base_dir = MEDIA_ROOT + '/banners'

    banner_dir_list = os.listdir(banners_base_dir)

    if curr_board in banner_dir_list:
       banner_dir_list.remove(curr_board)

    banner_dir  = random.choice(banner_dir_list)
    banner_file = random.choice(os.listdir(banners_base_dir + '/' + banner_dir))

    return {
        'url': '/' + banner_dir + '/',
        'img': '/static/banners/' + banner_dir + '/' + banner_file 
    }

def render_catalog(request, board_name):
    """
    Render catalog page for specific board.

    :param request: user's request
    :param board_name: board url
    :return: catalog page
    """
    boards = Board.objects.all().order_by('uri').values()
    try:
        board = [_ for _ in boards if _['uri'] == board_name][0]
    except IndexError:
        raise Http404("Board doesn't exist")
    threads = Post.objects.filter(board__uri=board['uri'], thread=None) \
        .order_by('-bump').values()[:100]
    posts = Post.objects.filter(board__uri=board['uri']).values('thread')
    for thread in threads:
        thread['reply_count'] = len([_ for _ in posts
                                     if _['thread'] == thread['id']])
    context = {
        'config': config,
        'board': board,
        'boards': boards,
        'recent_posts': threads,
        'hr': True
    }
    return render(request, 'posts/catalog.html', context)


def get_media(request, board_name, media_type, path):
    """Deal with media files (sic!)"""
    root = f'{MEDIA_ROOT}{media_type}/{board_name}'
    return serve(request, path, document_root=root)


def make_stats(data):
    """
    Count posting statistics.

    :param data: posts for statistics
    :return: Statistics object
    """

    class Statistic(object):
        """
        Object which contain next statistics data:

        Number of all posts;
        Number of posted threads;
        Number of posters;
        Variables with '_per24' suffix -- same thing but for last 24 hours
        """

        def __init__(self, posts):
            # functions for DRY
            def count_threads(_posts):
                """
                Count number of threads in _posts.

                :param _posts: source data
                :return: number of threads
                """
                return len([post for post in _posts if not post['thread']])

            def count_posters(_posts):
                """
                Count number of uniques posters in _posts.

                :param _posts: source data
                :return: number of posters
                """
                return len(set(post['ip'] for post in _posts))

            # getting time info
            past_24h = timezone.now() - timedelta(hours=24)
            past_90d = timezone.now() - timedelta(days=90)
            # total objects
            boards_posts = []
            for board in Board.objects.all().values():
                boards_posts.append(board['posts'])
            self.total_posts = sum(boards_posts)
            self.total_threads = count_threads(posts)
            self.posters = count_posters(posts)
            # stats for last 90 days
            last_posts = [
                {
                    'board_id': post['board_id'],
                    'time': post['time'].strftime('%y-%m-%d')
                }
                for post in posts if post['time'] >= past_90d]
            self.data = [
                [board['uri']] +
                [OrderedDict(
                    sorted(
                        Counter(
                            [
                                post['time']
                                for post in last_posts[::-1]
                                if post['board_id'] == board['id']
                            ]
                        ).items(),
                        key=lambda x: int(x[0].replace('-', ''))
                    )
                )]
                for board in Board.objects.all().values()]
            # objects for last 24 hours
            last_posts = [post for post in posts if post['time'] >= past_24h]
            self.posts_per24 = len(last_posts)
            self.threads_per24 = count_threads(last_posts)
            self.posters_per24 = count_posters(last_posts)

    stats = Statistic(data)
    return stats


def get_posts(board):
    """
    Return post's query.

    :param board: board %)
    :return: post's query
    """
    return Post.objects.filter(board=board)


def get_threads(posts):
    """
    Return threads objects.

    :param posts: data for filtering
    :return: threads query
    """
    return posts.filter(thread=None)


def get_ip(request):
    """
    Return a user ip.
    :param request: http/s request
    :return: ip address
    """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def get_board(board_uri):
    """
    TODO: rewrite

    :return: cached board object
    """
    return Board.objects.get(uri=board_uri)


def get_boards_navlist():
    """
    TODO: rewrite

    :return: board
    """
    boards = Board.objects.order_by('uri')
    return boards


def handle_delete(post_data, board, thread_id):
    for key in post_data.keys():
        if key.startswith('delete_'):
            post_id = key[7:]
            post_to_delete = Post.objects.get(
                board__id=board['id'],
                id=post_id
            )
            if post_to_delete.password == post_data['password']:
                post_to_delete.delete()
                return HttpResponseRedirect(
                    reverse('thread', args=[
                        board['uri'],
                        thread_id
                    ]))


def handle_report(request, board, thread_id):
    _reason = request.POST['reason']
    to_report = [_[7:] for _ in request.POST.keys() if _.startswith('delete_')]
    if len(to_report):
        report = Report(
            ip=get_ip(request),
            reason=_reason
        )
        report.save()
        for post in to_report:
            report.post.add(Post.objects.get(board__id=board['id'], id=post))
    return HttpResponseRedirect(
        reverse('thread', args=[board['uri'], thread_id]))


def handle_posting_response(new_post_id, json_response, thread_id, board_name):
    if new_post_id:
        if json_response:
            respond = json.dumps({
                'id': new_post_id,
                'noko': False,
                'redirect': '/' + board_name
            })
            answer = HttpResponse(
                respond,
                content_type="application/json"
            )
        else:
            anchor = '' if new_post_id == thread_id else f'#{new_post_id}'
            answer = HttpResponseRedirect(
                reverse('thread', args=[
                    board_name,
                    thread_id
                ]) + anchor
            )
        return answer
