import os
from datetime import datetime

import simplejson as json
from django.http import HttpResponse
from django.core.paginator import Paginator

from .models import Post


def board_threads_api(request, board_name):
    threads = Post.objects.filter(
        thread=None,
        board__uri=board_name
    ).order_by('-bump')
    pages = Paginator(threads, 10)
    respond = [{
        'threads': [
            {
                "no": thread.id,
                'last_modified': thread.bump
            } for thread in threads[(page-1)*10:page*10]
        ],
        'page': page
      } for page in pages.page_range]
    return HttpResponse(json.dumps(respond), content_type='application/json')


def board_page_api(request, board_name, page):
    page = int(page)
    threads = Post.objects.filter(
        thread=None,
        board__uri=board_name
    ).order_by('-bump')
    respond = {
        'threads': [
            {
                'posts': [
                    {
                        **{
                            "no": thread.id,
                            "com": thread.body,
                            "name": thread.name,
                            "time": datetime.timestamp(thread.time),
                            "omitted_posts": 0,
                            "omitted_images": 0,
                            "replies": Post.objects.filter(
                                board__uri=board_name,
                                thread=thread.id
                            ).count(),
                            "images": 0,
                            "sticky": 0,
                            "locked": 0,
                            "cyclical": "0",
                            "last_modified": datetime.timestamp(thread.bump),
                            "resto": 0
                        }, **generate_files_fields(thread)
                    }
                ]+[
                    {
                        **{
                            "no": post.id,
                            "com": post.body,
                            "name": post.name,
                            "time": datetime.timestamp(post.time),
                            "resto": post.thread
                        }, **generate_files_fields(post)
                    } for post in reversed(Post.objects.filter(
                        board__uri=board_name,
                        thread=thread.id
                    ).order_by('-id')[:5])]
            } for thread in threads[page*10:(page+1)*10]
        ]
    }
    return HttpResponse(json.dumps(respond), content_type='application/json')


def board_thread_api(request, board_name, thread_id):
    thread = Post.objects.get(id=thread_id, board__uri=board_name)
    respond = {'posts': [
                    {
                        **{
                            "no": thread.id,
                            "com": thread.body,
                            "name": thread.name,
                            "time": datetime.timestamp(thread.time),
                            "omitted_posts": 0,
                            "omitted_images": 0,
                            "replies": Post.objects.filter(
                                board__uri=board_name,
                                thread=thread.id
                            ).count(),
                            "images": 0,
                            "sticky": 0,
                            "locked": 0,
                            "cyclical": "0",
                            "last_modified": datetime.timestamp(thread.bump),
                            "resto": 0
                        }, **generate_files_fields(thread)
                    }]+[
                    {
                        **{
                            "no": post.id,
                            "com": post.body,
                            "name": post.name,
                            "time": datetime.timestamp(post.time),
                            "resto": post.thread
                        }, **generate_files_fields(post)
                    } for post in Post.objects.filter(
                        board__uri=board_name,
                        thread=thread_id
                    )]}
    return HttpResponse(json.dumps(respond), content_type='application/json')


def get_file(files):
    if isinstance(files, str):
        if files != '[]' and files != '':
            return json.loads(files)[0]
    return False


def generate_files_fields(post):
    file = get_file(post.files)
    if file:
        return {
            **{
                "tn_h": file['thumbheight'],
                "tn_w": file['thumbwidth'],
                "fsize": file['size'],
                "filename": (os.path.splitext(file['file']))[0],
                "ext": '.'+file['extension'],
                "tim": (os.path.splitext(file['file']))[0]
            }, **image_fields(file)
        }
    else:
        return {}


def image_fields(file):
    return {
        "h": file['height'],
        "w": file['width']
    } if file['is_an_image'] else {
        "h": 0,
        "w": 0
    }
