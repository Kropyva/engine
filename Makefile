MANAGE=./manage.py

test:
	flake8 --statistics --max-complexity 10 --exclude='.environ*, *migrations, manage.py, *__.py' .
	python $(MANAGE) test

run:
	python $(MANAGE) runserver 0.0.0.0:8000

run-prod:
	gunicorn config.wsgi \
		--name Kropyvaba \
		--bind 127.0.0.1:8080 \
		--workers=9 \
		--daemon

migrate:
	python $(MANAGE) makemigrations
	python $(MANAGE) migrate

collect:
	mkdir -p tools/jquery
	wget https://code.jquery.com/jquery-3.3.1.min.js -O tools/jquery/jquery-3.3.1.slim.min.js
	cp tools/dollchan/src/Dollchan_Extension_Tools.es6.user.js posts/static/dollchan.js
	cp tools/jquery/jquery-3.3.1.slim.min.js posts/static/jquery.js
	yes yes | python $(MANAGE) collectstatic

shell:
	python $(MANAGE) shell -i ipython

.PHONY: test migrate
