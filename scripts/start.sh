#!/bin/bash

currDir=$(pwd)
scriptDir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"

cd "$scriptDir/.."

source .environ/bin/activate
uwsgi --ini config/kropyvach_uwsgi.ini

deactivate

cd "$currDir"

exit

