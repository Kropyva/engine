#!/bin/bash

currDir=$(pwd)
scriptDir="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"

cd "$scriptDir/.."

/bin/bash scripts/stop.sh
/bin/bash scripts/start.sh

cd "$currDir"

exit

