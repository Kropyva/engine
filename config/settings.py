# encoding=utf-8

from pathlib import Path
from decouple import config as config_
from dj_database_url import parse as db_url
import raven
# import os

PROJECT_ROOT = Path(__file__).parent.parent

SETTINGS_DIR = Path(__file__).parent

SECRET_KEY = config_('SECRET_KEY', default='YOUR_SECRET')

PRODUCTION = config_('PRODUCTION', default=False, cast=bool)

STAGING = config_('STAGING', default=False, cast=bool)

# RAVEN_CONFIG = {
#        'dsn': 'https://96e19dc44dea455eaf77966269263dd1
#               :fc9c53061e584aedbdf35bc4712f57ad@sentry.io/216635',
#        'release': raven.fetch_git_sha(os.path.dirname(os.pardir)),
# }

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config_('DEBUG', default=False, cast=bool)


def get_list(value):
    return [s.strip() for s in value.split(',')]


ALLOWED_HOSTS = config_('ALLOWED_HOSTS', default='*', cast=get_list)

RAVEN_CONFIG = {
    'dsn': config_('RAVEN_DSN', default='YOUR_RAVEN_DSN'),
    # If you are using git, you can also automatically configure the
    # release based on the git info.
    'release': raven.fetch_git_sha(PROJECT_ROOT),
}

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'django.contrib.flatpages',
    'posts',
    'precise_bbcode',
]

if PRODUCTION:
    INSTALLED_APPS.append('raven.contrib.django.raven_compat')

if STAGING:
    INSTALLED_APPS.append('debug_toolbar')
    DEBUG_TOOLBAR_CONFIG = {
        'RESULTS_CACHE_SIZE': 100,
    }
    INTERNAL_IPS = config_('INTERNAL_IPS', default='127.0.0.1', cast=get_list)

SITE_ID = 1

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

if STAGING:
    MIDDLEWARE.append('debug_toolbar.middleware.DebugToolbarMiddleware')

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'config.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': config_(
        'DATABASE_URL',
        default='sqlite:///' + str(PROJECT_ROOT / 'db.sqlite3'),
        cast=db_url
    )
}

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

PASSW_VALIDATOR = 'django.contrib.auth.password_validation.'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': PASSW_VALIDATOR + 'UserAttributeSimilarityValidator',
    },
    {
        'NAME': PASSW_VALIDATOR + 'MinimumLengthValidator',
    },
    {
        'NAME': PASSW_VALIDATOR + 'CommonPasswordValidator',
    },
    {
        'NAME': PASSW_VALIDATOR + 'NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'uk'

TIME_ZONE = 'Europe/Kiev'

USE_I18N = True

USE_L10N = True

LOCALE_PATHS = [
    str(PROJECT_ROOT / 'locale')
]


USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_ROOT = str(PROJECT_ROOT / 'static') + '/'
STATIC_URL = '/static/'


# Media files

MEDIA_ROOT = '/var/www/kropyvach/media/'

config = {
    'title': "Кропивач",
    'url_favicon': "favicon.ico",
    'url_stylesheet': "style.css",
    'default_stylesheet': {
        '1': 'ukrchan.css'
    },
    'stylesheets': [
        ['Kropyvaba', 'style.css'],
        ['Ukrchan', 'ukrchan.css'],
        ['Futaba', 'futaba.css'],
	['Kropyvaba dark', 'kropyvaba_dark.css']
    ],
    'additional_javascript': [
        'jquery.js',
        'style-select.js',
    ],
    'footer': ['Кропивач 2016-2022'],
    'max_filesize': 25 * 1024 * 1024,  # 25MB
    'max_images': 4,
    'allowed_ext': ['png', 'jpeg', 'gif', 'jpg', 'webm', 'webp', 'mp4'],
    'uri_stylesheets': '',
    'font_awesome': True,
    'post_date': "%d-%m-%y о %H:%M:%S",
    'recent': "recent.css",
    'url_javascript': 'main.js',
    'wordfilter': [
        r'пилипони',
        r'[кk][0@aаoоοσ][кk][0@oоοσ]?[lлλ]',
        r'[hхxχ][0@aаoоοσіiι1][hхxχ][0@oоοσіiι1]?[lлλ]',
        r'[уyγ][сcς][рpρ][aа][їий][нH]',
        r'doxycycline-cheapbuy\.site',
#	r'[ж]\s?[иu]\s?[рpρթ]\s?[-oоοσ0](.*?)([мm]\s?[иu]\s?[рpρթ]|[дб]\s?[еeиaал])',
    r'[ж{Җ]\s?(\[(\/)?(.*?)\])?\s?[ϰиuυ]\s?(\[(\/)?(.*?)\])?\s?[Ƿрpρթ]\s?(\[(\/)?(.*?)\])?\s?[-oоοσ0ø](.*?)([мm]\s?(\[(\/)?(.*?)\])?\s?[ϰиuυ]\s?(\[(\/)?(.*?)\])?\s?[Ƿрpρթ]|[дбᵬ]\s?(\[(\/)?(.*?)\])?\s?[еeиaалрpρթ]|[йи́ū]\s?(\[(\/)?(.*?)\])?\s?[-oоοσ0ø])',	
	r'[Җж{][Ƿрpρթ][мmдdйб][рpρթеeоo0л]',
	r'[nнH][iі1][cс][h][aа][nн]',
	r'[нnH*][іi1*][ч4*][аaуy*][нn*]',
	r'[fф][рp][аa@][нn][кk][оoіi][тt][и][кk]',
	r'https://bit.ly/',
	r'[ф][р][н][к][т][к]',
	r'Отправим Ваше коммерческое предложение',
	r'<a href',
	r'[Ⓞ◯Ƨ]|\n{5,}?',
	r'rUANdTQGGzU'
			
    ],
    'catalog_link': 'catalog.html',
    'button_reply': "Надіслати",
    'button_newtopic': "Створити нитку",
    'allow_delete': True,
    'slogan': [
        "Український іміджборд",
        "Насирматри!",
        "... просто приклади до болючого місця",
        "Режисерська версія Січі",
        "Я ти дам колєґи, я ти дам рове́ри!",
        "Лікує буряк!",
        "Бордити по-новому!",
        "Завантаження…",
        "З коментарями Ґордона Фрімена!",
        "Відтепер безкоштовний!",
        "БЕЗ ГМО!",
        "Виготовлений з повторно використаних html теґів!",
        "[ОК]   [Скасувати]",
        "Передай привіт товаришу майору!",
        "Надає денну норму вітаміну К!",
        "Твій улюблений!",
        "Лише 10,44₴ на Аукро!",
        "Майбутня головна партія країни!",
        "Надто крутий, щоб бути справжнім!",
        "Від творців Кропивача!",
        "Без барвників та консервантів!",
        "Передбачений Ностардамусом!",
        "… садок зелений коло хати!",
        "За це відвідування ви отримали 10 кропиванців!",
        "Місце, де ти не хуй дурний!",
        "Викликає звикання!",
        "Додано досягнення та колекційні картки!",
        "Припечи свою рану!",
        "Для реєстрації натисни Ctrl+W",
        "Четверте з половиною диво України!",
        "Щоб продовжити, вкиньте монетку!",
        "Будь-яка схожість з іншими іміджбордами, \
            живими чи мертвими, є абсолютно випадковою!",
        "Ласкаво просимо в інтернет, курво!",
        "Ми крадемо у Футурами!",
        "Європа б ним пишалася, але він наш!",
        "Відтепер дводомний!",
        "Комарики-дзюбрики, Кропивач!",
        "MACHT FREI!",
        "Ґрантується держдепом США!",
        "Схвалено Трампом!",
        "МОЗ рекомендує!",
        "Часте відвідування знижує рівень холостерину в крові!",
        "Штаб диванних військ!",
        "У нас ніколи не болить голова!",
        "Zip-file!",
        "Очорнить який завгодно вересень!",
        "За МакНаґетса!",
        "Схвальуйе навіть йуродивий правопис!",
        "Денна норма зради!",
        "Без побічних ефектів!",
        "Місце, де ти можеш похвалитися своїм тетрісом!",
        "Відтепер із убудованою люлькою Сагайдачного!",
        "Із кольоровою статистикою!",
        "Без приміток!*",
        "Скажи «паляниця»!",
        "జ్ఞ‌ా",
		"Тихо шелестить у місячні весняні ночі!",
		"Вхід за посвідченням про відсутність особи!",
		"Придатний для супу!",
		"Лолі несила!",
		"Достатньо українське ньюфаження!",
		#"ЖРМРФРСР СМКТНЛ!",
		"З першого погляду!",
		"Для краси та здоров'я!"
    ]
}
