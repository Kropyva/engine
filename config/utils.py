# encoding=utf-8


def get_uptime():
    with open('/proc/uptime', 'r') as f:
        secs = round(float(f.readline().split()[0]))
        return f'Цілих {secs // (60*60*24)} днів без падінь!'
